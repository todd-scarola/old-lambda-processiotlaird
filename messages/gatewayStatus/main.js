'use strict';

exports.processData = async(event) => {

   //parse AWS shadow packet for Laird gateway
   //$aws/things/deviceId-354616090308151/shadow/update/accepted'
   
   //strip deviceId-
   event.gatewayId=event.gatewayId.slice(9);
   
   return event;
   
};
