'use strict';

exports.processData = async(event) => {

   //parse Laird packet
   var message = await this.parseMessage(event.messageBody);
   
   //add messageType
   message.messageType=event.messageType;

   //add timestamp
   message.processTime = Date.now();

   //add gateway info
   message.gatewayId = event.gatewayId;

   //parse battery
   message.batteryPercent = await this.parseBattery(Number(event.messageBody.batteryLevel));

   //parse range
   for (let i = 0; i < message.entries.length; i++) {
      var entry = message.entries[i];

      //loop through scans and aggregate consecutive scans
      for (let j = 0; j < entry.logs.length; j++) {
         var scan = entry.logs[j];
         if (scan.recordType == 17) {
            scan.log.range = await this.parseRange(scan.log.rssi);
         }
      }
   }

   //cleanup json ordering
   var entries = message.entries;
   delete message.entries;
   message.entries = entries;

   //include raw message
   message.raw = event.messageBody;


   return message;
};

exports.parseBattery = async(batteryLevel) => {
   if (batteryLevel > 3000) { return 100; }
   else if (2900 < batteryLevel && batteryLevel < 3000) { return 80; }
   else if (2850 <= batteryLevel && batteryLevel < 2900) { return 60; }
   else if (2800 <= batteryLevel && batteryLevel < 2850) { return 40; }
   else if (2700 <= batteryLevel && batteryLevel < 2800) { return 20; }
   else if (batteryLevel <= 100) { return batteryLevel; }
   else { return 10; }
};

exports.parseRange = async(rssi) => {
   if (-35 <= rssi && rssi <= 0) { return 0 } //very close
   if (-60 <= rssi && rssi < -35) { return 1 } //close
   if (-99 <= rssi && rssi < -60) { return 2 } //mid-range
   if (rssi < -99) { return 3 } //far
   return 999; //unknown
};


exports.parseMessage = async(data) => {
   const Parser = require('binary-parser').Parser;

   const reverseBytes = (input) => {
      let result = '';
      for (let i = 0; i < input.length; i = i + 2) {
         result = input.substr(i, 2) + result;
      }
      return result;
   };

   let buf = Buffer.from(data, 'base64');
   const protocolVersion = buf.readUInt16LE(0);
   // console.log(protocolVersion)

   // Start POM Parser 2
   // parses each 8 byte log
   const logParser8 = new Parser()
      .endianess('little')
      .seek(2) // 2 bytes reserved
      .uint16('delta') // scan count since first record timestamp
      .int8('rssi')
      .uint8('motion')
      .int8('txPower');

   // parses each 4 byte log
   const logParser4 = new Parser()
      .endianess('little')
      .int8('rssi')
      .uint8('motion')
      .int8('txPower');

   const logParser = new Parser()
      .endianess('little')
      .uint8('recordType')
      .choice('log', {
         tag: 'recordType',
         choices: {
            16: logParser4,
            17: logParser8
         }
      });

   // array of entries
   const entryParser = new Parser()
      // log entry header
      .endianess('little')
      .uint8('entryStart') //A5
      .uint8('flags')
      .uint16('scanInterval')
      .string('serial', {
         encoding: 'hex',
         length: 6,
         formatter: reverseBytes
      })
      .uint32('timestamp') //30
      .uint16('length'); //32

   if (protocolVersion === 2) {
      entryParser.array('logs', {
         type: logParser,
         lengthInBytes: function() {
            return this.length - 16;
         }
      });
   }
   else if (protocolVersion === 1) {
      entryParser.array('logs', {
         type: logParser,
         readUntil: 'eof'
      });
   }

   // parses the main payload
   const pomParser = new Parser()
      .endianess('little')
      // header
      .uint16('entryProtocolVersion') //2
      .string('deviceId', { //8
         encoding: 'hex',
         length: 6,
         formatter: reverseBytes
      })
      .uint32('deviceTime') //12
      .uint32('lastUploadTime'); //16

   const headerVersionCheckByte = buf.readUInt8(16);
   // For latest header version, add some fields
   // 0xA5 at pos 16 means this is prior v2 protocol
   // without battery_level, network_id and fw_version
   // otherwise we do add parsers for those fields here

   if (headerVersionCheckByte != 0xA5) {
      pomParser
         .string('fwVersion', { //16
            encoding: 'hex',
            length: 4
         })
         .uint8('batteryLevel', { formatter: function(val) { return (val << 4); } }) //20 (left-shift by 4 to re-scale to millivolts)
         .uint16('networkId'); //22
   }

   pomParser
      .array('entries', {
         type: entryParser,
         readUntil: 'eof'
      });

   try {
      let jsonPayload = pomParser.parse(buf);
      return jsonPayload;
   }
   catch (error) {
      console.error(error);
   }

};
