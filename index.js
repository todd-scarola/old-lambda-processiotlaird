const AWS = require('aws-sdk');
const SQS = new AWS.SQS();


//inboud message is an SQS trigger
exports.handler = async(event) => {

   var responses = [];

   //need to figure out which property to ID SQS messages
   if (event.hasOwnProperty('Records'))
   //SQS event
   {
      for (const record of event.Records) {
         var body = record.body;
         try {
            body = JSON.parse(body);
         }
         catch (e) { console.error(e); }

         responses.push(await this.processMessage(body));
      }
      return responses;
   }
   else
   //normal JSON request
   {
      return await this.processMessage(event);
   }
};


exports.processMessage = async(event) => {

   //get processing function
   var helper = null;
   if (event.hasOwnProperty('messageType')) {
      try {
         helper = require('./messages/' + event.messageType + '/main.js');
      }
      catch (error) {
         console.error(error);
      }
   }

   if (helper) {
      var message = await helper.processData(event);
      if (message) {
         //write to SQS
         if (await this.sendSqs(JSON.stringify(message))) {
            return message;
         }
      }
   }
};


//send message to SQS
exports.sendSqs = async function(message) {
   return new Promise(
      function(resolve, reject) {
         var params = {
            "MessageBody": message,
            "QueueUrl": process.env.SQS_URL
         };

         SQS.sendMessage(params, function(err, data) {
            if (err) {
               console.log('SQS error: %s', err);
               reject(err);
            }
            else {
               resolve(data);
            }
         });
      }
   );
};
